# Triángulo

Este programa plantea un algoritmo que permite determinar si un punto está al interior o exterior de un triángulo, esto se realiza mediante signalidad, calculando el área de un triángulo.

Las imágenes muestran el resultado, cuando el punto está dentro y fuera del triángulo.

![imagen1](https://framagit.org/Marcelo/triangulo/-/raw/main/Ejercicios/Im%C3%A1genes/ExteriorTri%C3%A1ngulo.png)
![imagen1](https://framagit.org/Marcelo/triangulo/-/raw/main/Ejercicios/Im%C3%A1genes/InteriorTri%C3%A1ngulo.png)
