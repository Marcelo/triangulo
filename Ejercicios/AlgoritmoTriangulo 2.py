# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Geometría Computacional
# Cálculo del área de un triángulo
# Descripción: De acuerdo al área del triángulo se define si un punto está dentro o fuera de el mismo

from OpenGL.GL import *
from OpenGL.GLU import *
import pygame
from pygame.locals import *
import sys, os, traceback
if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"
from math import *
import math
import numpy as np

pygame.display.init()
pygame.font.init()
#Screen configuration
screen_size = [800,600]
multisample = 0
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
pygame.display.set_caption("Triángulo y Punto")
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)
pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)

print(glGetIntegerv(GL_MAX_TEXTURE_SIZE))

glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST)
glEnable(GL_DEPTH_TEST)

camera_rot = [90.0,0.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0] #The sphere's center
In_X = 0
In_Y = 0

def get_input():
    global camera_rot, camera_radius, In_Y, In_X
    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()
    mouse_rel = pygame.mouse.get_rel()
    for event in pygame.event.get():
        if   event.type == QUIT: return False
        elif event.type == KEYDOWN:
            if   event.key == K_ESCAPE: return False
        elif event.type == MOUSEBUTTONDOWN:
            #Zoom in
            if   event.button == 4: camera_radius *= 0.9
            #Or out.
            elif event.button == 5: camera_radius /= 0.9
    if mouse_buttons[0]:
        camera_center[0] += mouse_rel[0] / 5
        camera_center[1] -= mouse_rel[1] / 5

    # mueve un puntero sobre la pantalla
    if sum(mouse_position) > 0:
        posX, posY = pygame.mouse.get_pos()
        In_X, In_Y = (posX-400)/10, (-posY+300)/10
    return True

def draw():
    glClearColor(1.0, 1.0, 1.0, 0.0)
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glViewport(0,0,screen_size[0],screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0])/float(screen_size[1]), 0.1,100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    camera_pos = [
        camera_center[0] + camera_radius*cos(radians(camera_rot[0]))*cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius                            *sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius*sin(radians(camera_rot[0]))*cos(radians(camera_rot[1]))
    ]
    gluLookAt(
        camera_pos[0],camera_pos[1],camera_pos[2],
        camera_center[0],camera_center[1],camera_center[2],
        0,1,0
    )

    #Se definen los puntos del triángulo
    #Pto 1
    ax, ay = -5, -3
    # Pto 2
    bx, by = 2, 3
    # Pto 3
    cx, cy = -6, 6

    #Se dibuja el triángulo
    glColor3f(1,0,0)
    #Start drawing triangles.  Each subsequent triplet of glVertex*() calls will draw one triangle.
    glBegin(GL_LINE_LOOP)
    glColor3f(1, 0, 0)
    glVertex3f(ax, ay, 0.0) #Make a vertex at (0.0,0.0,0.0)
    glVertex3f(bx, by, 0.0) #Make a vertex at (0.8,0.0,0.0)
    glVertex3f(cx, cy, 0.0)  # Make a vertex at (0.8,0.0,0.0)
    glEnd()

    #Se dibuja el punto
    glPushMatrix()
    glTranslatef(In_X,In_Y,0)
    glColor3f(0, 1, 0)
    glBegin(GL_POLYGON)
    for i in np.arange(0.0, 10.0, 0.1):
        x = 0.1 * math.cos(i)
        y = 0.1 * math.sin(i)
        glVertex3f(x, y, 0)
    glEnd()
    glPopMatrix()

    #Se realizan los cálculos para saber el sentido del triángulo
    R = 0.5*(ax*(by-cy) + bx*(cy-ay) + cx*(ay-by))
    #print(R)

    # Se realizan los cálculos para saber el sentido del triángulo 1
    R1 = 0.5 * (ax * (by - In_Y) + bx * (In_Y - ay) + In_X * (ay - by))
    #print('R1'+str(R*R1))
    # Se realizan los cálculos para saber el sentido del triángulo 2
    R2 = 0.5 * (bx * (cy - In_Y) + cx * (In_Y - by) + In_X * (by - cy))
    #print('R2'+str(R*R2))
    # Se realizan los cálculos para saber el sentido del triángulo 3
    R3 = 0.5 * (cx * (ay - In_Y) + ax * (In_Y - cy) + In_X * (cy - ay))
    #print('R3'+str(R*R3))

    #Se verifica si el punto esta dentro o fuera del triángulo
    if (R*R1>0.0) and (R*R2>0.0) and (R*R3>0.0):
        print("Está dentro del triángulo")
        glBegin(GL_TRIANGLES)
        glColor3f(0, 0, 1)
        glVertex3f(ax, ay, 0.0)  # Make a vertex at (0.0,0.0,0.0)
        glVertex3f(bx, by, 0.0)  # Make a vertex at (0.8,0.0,0.0)
        glVertex3f(cx, cy, 0.0)  # Make a vertex at (0.8,0.0,0.0)
        glEnd()
    else:
        print("Está fuera del triángulo")

    #print('X:' + str(In_X) + " Y:" + str(In_Y))

    pygame.display.flip()

def main():
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        draw()
        clock.tick(60) #Regulate the framerate to be as close as possible to 60Hz.
    pygame.quit()

if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()